const path = require('path');
const HTMLPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.ts',
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, 'dist'),
    },
    devServer: {
        port: 4300,
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/,
        },{
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        },{
            test: /\.svg$/,
            type: 'asset/resource',
                generator: {
                filename: path.join('icons', '[name].[contenthash][ext]'),
            },
        },]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.css', '.svg']
    },
    plugins: [
        new HTMLPlugin({
            template: './src/index.html'
        })
    ]
}