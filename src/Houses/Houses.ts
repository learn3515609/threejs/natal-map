import * as THREE from 'three';
import { THouse, houses } from "./data";

export class Houses {
    static lineRadius = 0.01;
    static lineSegments = 12;
    static lineHeight = 1.5;
    static size = Math.PI / 6;
    
    static discRadius = 2.15;
    static discSegments = 75;

    static defaultColor = 'rgb(112,112,112)';

    static createHouse(house: THouse, i: number) {
        const group = new THREE.Group();
        const color = i % 3 != 0 ? this.defaultColor : 'red';
        const tubeMaterial = new THREE.MeshStandardMaterial({ color, metalness: 0.1, roughness: 0, side: THREE.DoubleSide})
        const tubeGeometry = new THREE.CylinderGeometry(this.lineRadius, this.lineRadius, this.lineHeight, this.lineSegments);
        const tube = new THREE.Mesh(tubeGeometry, tubeMaterial);
        tube.rotation.z = Math.PI / 2
        tube.position.x = this.lineHeight / 2 + this.discRadius;
        group.add(tube);
        return group;
    }

    static createDisc() {
        const discMaterial = new THREE.MeshStandardMaterial({ color: this.defaultColor, wireframe: true})
        const discGeometry = new THREE.TorusGeometry(this.discRadius,  this.lineRadius, 50, 50);
        const disc = new THREE.Mesh(discGeometry, discMaterial);
        //disc.rotation.x = Math.PI / 2;
        return disc;
    }

    static render() {
        const group = new THREE.Group();

        group.add(this.createDisc())

        for(let i = 0; i < 12; i++) {
            const mesh = this.createHouse(houses[i], i);
            mesh.rotation.z = this.size * i
            group.add(mesh);  
        }
        return group
    }
}