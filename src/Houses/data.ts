export type THouse = {
    name: string,
    angle: number
}

export const houses: THouse[] = [{
    name: 'ASC',
    angle: 145680
},{
    name: 'II',
    angle: 153680
},{
    name: 'III',
    angle: 235680
},{
    name: 'IC',
    angle: 45680
},{
    name: 'V',
    angle: 55680
},{
    name: 'VI',
    angle: 285680
},{
    name: 'DSC',
    angle: 155680
},{
    name: 'VIII',
    angle: 235680
},{
    name: 'IX',
    angle: 235680
},{
    name: 'MC',
    angle: 45680
},{
    name: 'XI',
    angle: 55680
},{
    name: 'XII',
    angle: 285680
},]