
import aries from './icons/aries.svg';
import taurus from './icons/taurus.svg';
import gemini from './icons/gemini.svg';
import cancer from './icons/cancer.svg';
import leo from './icons/leo.svg';
import virgo from './icons/virgo.svg';
import libra from './icons/libra.svg';
import scorpio from './icons/scorpio.svg';
import sagittarius from './icons/sagittarius.svg';
import capricorn from './icons/capricorn.svg';
import aquarius from './icons/aquarius.svg';
import pisces from './icons/pisces.svg';

export type TSign = {
    icon: string,
    color: string
}

export const signs: TSign[] = [{
    icon: pisces,
    color: 'rgb(100, 181, 246)'
},{
    icon: aries,
    color: 'rgb(255, 138, 101)'
},{
    icon: taurus,
    color: 'rgb(174, 213, 129)'
},{
    icon: gemini,
    color: 'rgb(149, 117, 205)'
},{
    icon: cancer,
    color: 'rgb(100, 181, 246)'
},{
    icon: virgo,
    color: 'rgb(255, 138, 101)'
},{
    icon: leo,
    color: 'rgb(174, 213, 129)'
},{
    icon: libra,
    color: 'rgb(149, 117, 205)'
},{
    icon: scorpio,
    color: 'rgb(100, 181, 246)'
},{
    icon: sagittarius,
    color: 'rgb(255, 138, 101)'
},{
    icon: capricorn,
    color: 'rgb(174, 213, 129)'
},{
    icon: aquarius,
    color: 'rgb(149, 117, 205)'
}]
