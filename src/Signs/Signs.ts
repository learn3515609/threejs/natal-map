import * as THREE from 'three';
import { SVGLoader } from 'three/examples/jsm/loaders/SVGLoader';
import { TSign, signs } from './data';

const loader = new SVGLoader();

export class Signs {
    static radius =  3;  
    static tubeRadius =  0.125;  
    static radialSegments = 15;  
    static tubularSegments =  15;  
    static size = Math.PI / 6;
    static gap = (Math.PI / 180) * 1.125;

    static createMesh(sign: TSign, index: number){
        const tubeMaterial = new THREE.MeshStandardMaterial({ color: sign.color, metalness: 0.1, roughness: 0, side: THREE.DoubleSide})
        const tubeGeometry = new THREE.TorusGeometry(this.radius, this.tubeRadius, this.radialSegments, this.tubularSegments, this.size - this.gap );
        const tube = new THREE.Mesh(tubeGeometry, tubeMaterial);

        const svgSize = this.tubeRadius;
        const svgSize2 = this.tubeRadius / 12;

        loader.load(
            sign.icon,
            (data) => {
                const paths = data.paths;
                const material = new THREE.MeshBasicMaterial( {
                    color: 'white',
                    side: THREE.DoubleSide,
                    depthWrite: false
                } );
                
                const svgGroup = new THREE.Group();
                for ( let i = 0; i < paths.length; i ++ ) {
                    const path = paths[ i ];
                    
                    const shapes = SVGLoader.createShapes( path );
                    for ( let j = 0; j < shapes.length; j ++ ) {
                        const shape = shapes[ j ];
                        const geometry = new THREE.ExtrudeGeometry( shape, {
                            depth: 2
                        } );
                        const mesh = new THREE.Mesh( geometry, material );
                        mesh.scale.set(svgSize2, svgSize2, svgSize2);
                        const box = new THREE.Box3().setFromObject(mesh);
                        const size = new THREE.Vector3();
                        box.getSize(size);

                        const xOffset = size.x / -2;
                        const yOffset = size.y / -2;
                        mesh.scale.y *= -1;
                        mesh.position.set(xOffset, -yOffset, 0);
                        svgGroup.add( mesh );
                    }
                }
                index === 0 && group.add(svgGroup);

                svgGroup.position.z = this.tubeRadius * 0.9;
                svgGroup.position.x = this.radius * Math.cos(this.size / 2);
                svgGroup.position.y = this.radius * Math.sin(this.size / 2);
                svgGroup.rotation.z = - (Math.PI / 2 - this.size / 2);

                group.add(svgGroup);

            }
        )
        
        const group = new THREE.Group();
        group.add(tube);
        return group;
    }

    static render() {
        const group = new THREE.Group();
        for(let i = 0; i < 12; i++) {
            const mesh = this.createMesh(signs[i], i);
            mesh.rotation.z = this.size * i + this.gap/2;
            group.add(mesh);  
        }
        return group
    }
}


// const planeMaterial = new THREE.MeshStandardMaterial({ color: sign.color, metalness: 0, roughness: 0.5, side: THREE.DoubleSide})
// const planeGeometry = new THREE.CircleGeometry(this.tubeRadius, this.radialSegments);
// const planeStart = new THREE.Mesh(planeGeometry, planeMaterial);
// planeStart.rotation.x = Math.PI / 2;
// planeStart.position.x = Math.PI - this.tubeRadius;
// const planeEnd = new THREE.Mesh(planeGeometry, planeMaterial);
// planeEnd.rotation.x = Math.PI / 2;
// planeEnd.rotation.z = 0.1;
//const planeS