import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import './style.css';
import Stats from 'stats.js';
import { Signs } from './Signs/Signs';
import { Houses } from './Houses/Houses';



const pixelRatio = 1;

const canvas = document.getElementsByTagName('canvas')[0];
const renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight);
camera.position.z = 6;

// const axesHelper = new THREE.AxesHelper( 5 );
// scene.add( axesHelper );

const controls = new OrbitControls(camera, canvas);
const stats = new Stats();
stats.showPanel(0);
document.body.appendChild(stats.dom);

const hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.8);
hemiLight.position.set(0, 50, 0);
scene.add(hemiLight);

const dirLight = new THREE.DirectionalLight(0xffffff, 0.8);
dirLight.position.set(-8, 12, 8);
dirLight.castShadow = true;
dirLight.shadow.mapSize = new THREE.Vector2(1024, 1024);
scene.add(dirLight)

scene.add(Signs.render());
scene.add(Houses.render());

renderer.setSize(window.innerWidth * pixelRatio, window.innerHeight * pixelRatio);

const tick = () => {
    stats.begin();
    controls.update();
    renderer.render(scene, camera);
    stats.end();
    window.requestAnimationFrame(tick);
}

const onResize = () => {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth * pixelRatio, window.innerHeight * pixelRatio);
    renderer.render(scene, camera);
}

window.addEventListener('resize', onResize)
tick();